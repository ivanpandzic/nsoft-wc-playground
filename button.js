class Button extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.innerHTML = `
      <button class="my-button">Plain WC Button</button>
    `;
  }
  connectedCallback() { 
    this.shadowRoot.querySelector(".my-button").addEventListener('click', () => {
      alert('You clicked plain WC btn!');
    });
  }
  attributeChangeCallback() { }
  disconnectedCallback() {  }
}
customElements.define('my-button',Button);