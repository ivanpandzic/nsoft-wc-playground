import { Component } from '@angular/core';

@Component({
  selector: 'my-angular-button',
  templateUrl: './button.component.html'
})
export class MyAngularComponent {
  title = 'my-angular-button';

  echo() {
    alert("You clicked Angular button!")
  }
}
