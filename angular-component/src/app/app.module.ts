import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { MyAngularComponent } from './button.component';

@NgModule({
  declarations: [
    MyAngularComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [MyAngularComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const element = createCustomElement(MyAngularComponent, { injector: this.injector });
    customElements.define('my-angular-button', element);
  }

}
