##Installation

####Angular component
`cd angular-component`
`npm install`
`npm run build:elements`

####Vue component
`cd vue-component`
`npm install`
`npm run build:elements`

##Run

Open index.html and voila

##Reference

####Angular
https://blog.angulartraining.com/tutorial-how-to-create-custom-angular-elements-55aea29d80c5

####Vue
https://vuejsdevelopers.com/2018/05/21/vue-js-web-component/

##ToDo
Resarch https://stenciljs.com/docs/introduction/

##Requirements
Node version > 8.0.0